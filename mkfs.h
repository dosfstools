/******************************************************************************
 * @file            parted.h
 *****************************************************************************/
#ifndef     _PARTED_H
#define     _PARTED_H

struct mkfs_state {

    const char *boot, *outfile;
    char label[12];
    
    int create, size_fat, size_fat_by_user, verbose;
    unsigned long sectors, offset;
    
    unsigned char sectors_per_cluster;
    int chs_align;

};

extern struct mkfs_state *state;
extern const char *program_name;

#endif      /* _PARTED_H */
