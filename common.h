/******************************************************************************
 * @file            common.h
 *****************************************************************************/
#ifndef     _COMMON_H
#define     _COMMON_H

#define     LFN_MAX                     255

struct fat_dirent {

    unsigned char fn[12];
    unsigned short lfn[LFN_MAX];

};

int create_name (struct fat_dirent *dp, const char *path);
void gen_numname (unsigned char *dst, const unsigned char *src, const unsigned short *lfn, unsigned int seq);

int cmp_lfn (const unsigned short *lfnbuf, unsigned char *dir);
unsigned char sum_sfn (unsigned char *dir);

unsigned short generate_datestamp (void);
unsigned short generate_timestamp (void);

#endif      /* _COMMON_H */
