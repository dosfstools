/******************************************************************************
 * @file            mmd.h
 *****************************************************************************/
#ifndef     _MMD_H
#define     _MMD_H

struct mmd_state {

    char **dirs;
    long nb_dirs;
    
    const char *outfile;
    unsigned long offset;
    
    int chs_align;

};

#endif      /* _MMD_H */
