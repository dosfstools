/******************************************************************************
 * @file            mcopy.h
 *****************************************************************************/
#ifndef     _MCOPY_H
#define     _MCOPY_H

struct mcopy_state {

    char **files;
    long nb_files;
    
    int status;
    
    const char *outfile;
    size_t offset;
    
    int chs_align;

};

#endif      /* _MCOPY_H */
